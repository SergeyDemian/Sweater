package main.java.dska.controllers;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import main.java.dska.entyties.Role;
import main.java.dska.entyties.Roles;
import main.java.dska.repositories.RoleRepo;

@Controller
public class RegistrationController {

	@Autowired
	private RoleRepo roleRepo;
	
	@GetMapping("/registration")
	public String registration() {
		return "registration";
	}
	
	@PostMapping("/registration")
	public String addUser(Roles roles, Map<String, Object> model) {
		Roles rolesFromDB = roleRepo.findByUsername(roles.getUsername());
		if (rolesFromDB != null) {
			model.put("message", "User exists");
			return "registration";
		} 
		
		roles.setActive(true);
		roles.setRoles(Collections.singleton(Role.USER));
		roleRepo.save(roles);
		return "redirect: /login";
	}
}
