package main.java.dska.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import main.java.dska.entyties.User;
import main.java.dska.repositories.UserRepository;

@Controller
public class MainController {
	@Autowired
	private UserRepository userRepo;
	
	@GetMapping("/")
	public String greeting (Map<String, Object> model) {
		
		return "greeting";
		
	}
	
	@GetMapping("/main")
	public String main (Map<String, Object> model) {
		Iterable<User> users = userRepo.findAll();
		model.put("users", users);
		return "main";
	}
	
	@PostMapping("/main")
	public String add (@RequestParam(
									name = "name",
									required = true
									) String name, @RequestParam (
																	name = "email",
																	required = true
																	) String email, Map<String, Object> model) {
		User user = new User(name, email);
		userRepo.save(user);
		Iterable<User> users = userRepo.findAll();
		model.put("users", users);
		return "main";
	}
	
	@PostMapping("filter")
	public String findFilter(@RequestParam(
											name = "filter"
											) String filterTag, Map<String, Object> model) {
		Iterable<User> users;
		if (filterTag != null && !filterTag.isEmpty()) {
			
			users = userRepo.findByEmailContaining(filterTag);
			
		} else {
			
			users = userRepo.findAll();
			
		}
		
		model.put("users", users);
		
		return "main";
		}
	

}
