package main.java.dska.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import main.java.dska.entyties.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	List<User> findByEmailContaining(String email);
}
