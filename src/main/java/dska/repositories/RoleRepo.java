package main.java.dska.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import main.java.dska.entyties.Roles;

public interface RoleRepo extends JpaRepository <Roles, Long> {

	Roles findByUsername(String username);
}
