package main.java.dska.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;
	@Override
    protected void configure(HttpSecurity http) throws Exception {
       http
       		.authorizeRequests()
       			.antMatchers("/","/registration").permitAll()
       			.anyRequest().authenticated()
       			.and()
       		.formLogin()
       			.loginPage("/login")
       			.permitAll()
       			.and()
       		.logout()
       			.permitAll();
    }

    @SuppressWarnings("deprecation")
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.jdbcAuthentication()
    		.dataSource(dataSource)
    		.passwordEncoder(NoOpPasswordEncoder.getInstance())
    		.usersByUsernameQuery("select username, password, active from roles where username=?")
    		.authoritiesByUsernameQuery("select rs.username, r.roles from roles rs inner join user_role r on rs.id = r.role_id where rs.username = ?");
    }
}
